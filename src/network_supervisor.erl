-module(network_supervisor).
-export([start/4]).

start( address, port, game_pid, server_pid ) ->
	{ok, Lsocket} = gen_tcp:listen(port, [binary]),
	server( Lsocket ).

server( LSocket ) ->
	case gen_tcp:accept(LSocket) of
			{ok, socket} ->
				loop(socket, game_pid, server_pid),
				server(LSocket);
			Other ->
				io:format("accept returned"),
				ok	
	end.

loop( socket, game_pid, server_pid )->
	inet:setopts(socket,[{active,once}]),
    receive
		{tcp,socket,Data} ->
			io:format( Data ),
			gen_tcp:send("answer" , socket ),
			loop( socket, game_pid, server_pid );
		{tcp_closed,socket} ->
			io:format("Socket ~w closed [~w]~n",[socket,self()]),
            ok
    end.
